import { HttpErrorResponse, HttpHandler, HttpHeaderResponse, HttpInterceptor, HttpProgressEvent, HttpRequest, HttpResponse, HttpSentEvent, HttpUserEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError} from 'rxjs/operators';
import {DialogService} from "@root/modules/dialog/dialog.service";
import {AuthGuard} from "@srv/auth.service";

@Injectable()
export class UnauthInterceptor implements HttpInterceptor {

    constructor(
        private _authService: AuthGuard,
        private _dialogService: DialogService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse |
        HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        if (req.urlWithParams.indexOf('/assets/') !== -1) {
            return next.handle(req);
        }
        return next.handle(req).pipe(
            catchError(error => {
                console.log(error);
                if (error instanceof HttpErrorResponse) {
                    switch ((<HttpErrorResponse>error).status) {
                        case 401:
                            return this.handle401Error(req, next);
                        case 0:
                            return this.handle0Error(req, next);
                        default:
                            return throwError(error);
                    }
                } else {
                    return throwError(error);
                }
            }));
    }
    handle0Error(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req);
    }
    handle401Error(req: HttpRequest<any>, next: HttpHandler) {
        this._dialogService.openAlert({
            title: "Error",
            text: "You need to authorization",
            buttons: [
                {
                    title: "Close"
                }
            ]
        });
        this._authService.logout();
        return next.handle(req);
    }
}
