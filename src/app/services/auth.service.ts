import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { StorageService } from "@srv/storage.service";
import { AppConfig } from "@srv/config.service";
import { Observable } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanLoad {
    constructor(
      private _router: Router,
      private _ls: StorageService,
      private _config: AppConfig
	) {}

	canLoad(): Promise<boolean> {
	    if (!this._config.isAuth.value) {
            this.logout();
            return Promise.resolve(false);
        }
        return Promise.resolve(true);
	}

	logout() {
        this._config.isAuth.next(false);
        this._ls.removeData("UserToken");
        this._router.navigate(['login']);
    }
}
