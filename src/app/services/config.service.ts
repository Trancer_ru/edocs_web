import { Injectable } from "@angular/core";
import {BehaviorSubject} from "rxjs";

@Injectable()
export class AppConfig {
    isAuth = new BehaviorSubject<boolean>(false);
    baseUrl: string = "";
    siteUrl: string = "";
    isMobile: boolean = undefined;
    deviceType: 'mobile'|'tablet'|'desktop' = 'desktop';
}
