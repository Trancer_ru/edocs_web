import { Injectable } from "@angular/core";
import { AppConfig } from "./config.service";
import { StorageService } from "./storage.service";
import {BehaviorSubject} from "rxjs";


@Injectable()
export class HelpersService {
    constructor(
        private _ls: StorageService,
        private _config: AppConfig) { }

    docAdded$ = new BehaviorSubject<string>(undefined);
};
