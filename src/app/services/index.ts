import { AuthGuard } from "./auth.service";
import { AppConfig } from "./config.service";
import { HelpersService} from "./helpers.service";
import { StorageService} from "./storage.service";
import {UnauthInterceptor} from "./unauth.interceptor";

export const SERVICES = [
    AuthGuard,
    AppConfig,
    HelpersService,
    StorageService,
    UnauthInterceptor
]
