import { trigger, state, style, animate, query, transition, group } from "@angular/animations";

export const fadeIn = [
	trigger("fadeIn", [
		transition(":enter", [
			style({ opacity: 0 }),  // initial
			animate(".2s cubic-bezier(.8, 0.2, 0.6, 1.5)",
				style({ opacity: 1 }))  // final
		]),
		transition(":leave", [
			style({ opacity: 1 }),  // initial
			animate(".2s cubic-bezier(.8, 0.2, 0.6, 1.5)",
				style({ opacity: 0 }))// final
		])
	])
];

export const routerFadeIn =
	trigger("routerFadeIn", [
		transition('* <=> *', [
			query(':enter, :leave',
				style({ position: 'absolute', width: 'calc(100% - 50px)'}),
				{ optional: true }),
			group([  // block executes in parallel
				query(':enter', [
					style({ opacity: 0 }),
					animate('.3s ease-in-out', style({ opacity: 1 }))
				], { optional: true }),
				query(':leave', [
					style({ opacity: 1 }),
					animate('0.3s ease-in-out', style({ opacity: 0 }))
				], { optional: true }),
			])
		])
	]);
export const routerFadeInMobile =
	trigger("routerFadeInMobile", [
		transition('* <=> *', [
			query(':enter, :leave', style({ position: 'absolute', width: '100%'})
				, { optional: true }),
			group([  // block executes in parallel
				query(':enter', [
					style({ opacity: 0 }),
					animate('.3s ease-in-out', style({ opacity: 1 }))
				], { optional: true }),
				query(':leave', [
					style({ opacity: 1 }),
					animate('0.3s ease-in-out', style({ opacity: 0 }))
				], { optional: true }),
			])
		])
	]);

export const slideIn = [
	trigger('slideIn', [
		state('in', style({
			'max-height': '350px', 'min-height': '200px',  'opacity': '1', 'visibility': 'visible'
		})),
		state('out', style({
			'max-height': '0px', 'min-height': '0px', 'opacity': '0', 'visibility': 'hidden'
		})),
		transition('in => out', [group([
				animate('300ms 300ms ease-in-out', style({
					'opacity': '0'
				})),
				animate('300ms ease-in-out', style({
					'max-height': '0px', 'min-height': '0px'
				})),
				animate('300ms 300ms ease-in-out', style({
					'visibility': 'hidden'
				}))
			]
		)]),
		transition('out => in', [group([
				animate('1ms ease-in-out', style({
					'visibility': 'visible'
				})),
				animate('300ms ease-in-out', style({
					'max-height': '350px', 'min-height': '200px'
				})),
				animate('1ms ease-in-out', style({
					'opacity': '1'
				}))
			]
		)])
	]),
]

