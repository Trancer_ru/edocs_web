import { Injectable } from "@angular/core";
import { IAlertContent } from "./dialog.interface";
import { MatDialog } from "@angular/material";
import { AlertComponent } from "./alert/alert.component";
import { AccessedInfoComponent } from "@modules/accessed-info/accessed-info.component";
import { AddNewDocComponent } from "@modules/add-new-doc/add-new-doc.component";

@Injectable()
export class DialogService {

    constructor(
        private _dialog: MatDialog
    ){}

    openAlert(data: IAlertContent){
        this._dialog.open(AlertComponent, {
            data: data
        });
    }

    openModule(moduleName: string, data?: any) {
        let config =  {data: data, width: '90%'};
        let dialogRef;
        if(moduleName === 'accessed-info') dialogRef = this._dialog.open(AccessedInfoComponent, config);
        else if(moduleName === 'add-new-doc') dialogRef = this._dialog.open(AddNewDocComponent, config);
        return dialogRef.afterClosed();
    }
}
