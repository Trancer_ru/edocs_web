import { CommonModule } from "@angular/common";
import {NgModule, ModuleWithProviders} from "@angular/core";
import { AlertComponent } from "./alert/alert.component";
import {DialogService} from "./dialog.service";
import {MaterialDesignModule} from "@root/material.module";

import { AccessedInfoModule } from "@modules/accessed-info/accessed-info.module";
import { AccessedInfoComponent } from "@modules/accessed-info/accessed-info.component";
import {AddNewDocModule} from "@root/modules/add-new-doc/add-new-doc.module";
import {AddNewDocComponent} from "@root/modules/add-new-doc/add-new-doc.component";

@NgModule({
    imports: [CommonModule, MaterialDesignModule, AccessedInfoModule, AddNewDocModule],
    declarations: [AlertComponent],
    entryComponents: [AlertComponent, AccessedInfoComponent, AddNewDocComponent]
})

export class DialogModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: DialogModule,
            providers: [DialogService]
        }
    }
}
