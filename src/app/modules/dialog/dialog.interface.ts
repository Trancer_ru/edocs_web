export interface IAlertButton {
    title?: string;
    callback?: Function;
}

export interface IAlertContent {
    title?: string;
    text?: string;
    buttons?: IAlertButton[];
}
