import { APP_INITIALIZER, NgModule } from "@angular/core";
import { InitService } from "./init.service";

export function init_app(appLoadService: InitService) {
	return () => {
		return new Promise((resolve, reject) => {
			appLoadService.InitializeApp().subscribe(
				success => resolve(),
				error => {
					console.log("Нет логина");
					reject()
				}
			)
		});

	}
}

@NgModule({
	providers: [
		InitService,
		{ provide: APP_INITIALIZER, useFactory: init_app, deps: [InitService], multi: true }
	]
})
export class InitModule { }
