import { HttpClient } from "@angular/common/http";
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {BehaviorSubject, Subject, throwError, Observable} from "rxjs";
import { map, takeUntil, tap } from "rxjs/operators";
import {Document} from "@models/document";
import {DialogService} from "@root/modules/dialog/dialog.service";
import {catchError, filter} from "rxjs/internal/operators";
import {HelpersService} from "@srv/helpers.service";
import {AppConfig} from "@srv/config.service";


@Component({
	selector: "docs-list",
	templateUrl: "docs-list.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocsListComponent implements OnDestroy, OnInit {

    total = 0;
    take: number = 10;
    private _skip: number = 0;

    docs$ = new BehaviorSubject<Document[]>(undefined);
    filename$: Observable<string>;
    filenamePrefix: string = this._config.siteUrl;
    private _destroyed$: Subject<boolean> = new Subject<boolean>();
    displayedColumns = [
        'link',
        'user',
        'fileName',
        'description',
        'addressee',
        'expirationDate',
        'sameIp',
        'opened',
        'accessed'
    ];

	constructor(
	    private _config: AppConfig,
	    private _http: HttpClient,
        private _helpers: HelpersService,
        private _dialogService: DialogService

	) {}

	ngOnInit() {
        this.getDocsList();
        this.filename$ = this._helpers.docAdded$
            .pipe(
                filter(value => Boolean(value)),
                tap(_ => this._reloadList()),
            )
	}

	getDocsList(){
        this._http.get<{total: number, docs: Document[]}>(`{{api}}/docs-list?take=${this.take}&skip=${this._skip}`)
            .pipe(
                takeUntil(this._destroyed$),
                tap( data => this.total = data.total),
                map( data => data.docs.map(item => new Document(item))),
                catchError(err => {
                    this._dialogService.openAlert({
                        title: "Error",
                        text: "An error occurred while getting the list of documents",
                        buttons: [
                            {title: "Close"}
                        ]
                    });

                    return throwError(err);
                })
            )
            .subscribe( data => {
                this.docs$.next(data);
            });
    }

    changePage(event: any){
        this._skip = this.take * event.pageIndex;
        this.getDocsList();
    }

	showAccessed(id: string) {
	    this._dialogService.openModule('accessed-info', {id: id});
    }

    showLink(name: string) {
        this._dialogService.openAlert({
            title: `${this.filenamePrefix}/get-document/${name}`,
            buttons: [
                {title: "Close"}
            ]
        });
    }

    private _reloadList(){
	    this._skip = 0;
	    this.getDocsList();
    }

    ngOnDestroy() {
        this._destroyed$.next(true);
        this._destroyed$.complete();
    }
}
