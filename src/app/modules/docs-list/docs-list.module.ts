import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { DocsListComponent } from "./docs-list.component";
import { DocsListRouter } from "./docs-list.router";
import { MaterialDesignModule } from "@root/material.module";
import { DialogModule } from "@modules/dialog/dialog.module";
import {MomentModule} from "ngx-moment";

@NgModule({
	imports: [
		CommonModule,
        DocsListRouter,
        MaterialDesignModule,
        DialogModule.forRoot(),
        MomentModule
	],
	declarations: [
		DocsListComponent,
	]
})
export class DocsListModule { }
