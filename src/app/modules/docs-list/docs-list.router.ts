import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DocsListComponent } from "./docs-list.component";


const routes: Routes = [
	{ path: "", component: DocsListComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class DocsListRouter { }



