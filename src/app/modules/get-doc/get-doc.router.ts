import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GetDocComponent } from "./get-doc.component";


const routes: Routes = [
	{ path: "", component: GetDocComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: []
})
export class GetDocRouter { }



