import { HttpClient } from "@angular/common/http";
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from "@angular/core";
import { Observable, Subject, throwError, of, forkJoin, merge } from "rxjs";
import {AppConfig} from "@srv/config.service";
import {GetDocument} from "@root/models/get-document";
import {DialogService} from "@root/modules/dialog/dialog.service";
import {catchError, map} from "rxjs/internal/operators";
import {ActivatedRoute} from "@angular/router";

@Component({
	selector: "get-doc",
	templateUrl: "get-doc.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class GetDocComponent implements OnDestroy, OnInit {
    isLoading: boolean = false;
    private _destroyed$: Subject<boolean> = new Subject<boolean>();
    errorStatus: number = 200;

    getFile$: Observable<string>;
	constructor(
        private _activatedRoute: ActivatedRoute,
        private _config: AppConfig,
        private _http: HttpClient,
        private _dialogService: DialogService
	) {}

    ngOnInit() {
        this.getFile$ = this._activatedRoute.params.pipe(
            map(params => `${this._config.baseUrl}/get-doc/${params.filename}`)
        );
    }

    getFile(filename: string) {
        return this._http.get<GetDocument>(`{{api}}/get-doc/${filename}`)
            .pipe(
                map( data => new GetDocument(data)),
                catchError(err => {
                    this._dialogService.openAlert({
                        title: "Error",
                        text: "An error occurred while getting the file",
                        buttons: [
                            {title: "Close"}
                        ]
                    });

                    return throwError(err);
                })
            )
    }

    onError(e){
	    console.log(e);
	    this.errorStatus = e.status;
    }

	ngOnDestroy() {
		this._destroyed$.next(true);
		this._destroyed$.complete();
	}
}
