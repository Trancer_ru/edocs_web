import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { GetDocComponent } from "./get-doc.component";
import { GetDocRouter } from "./get-doc.router";
import { MaterialDesignModule } from "@root/material.module";
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {DialogModule} from "@root/modules/dialog/dialog.module";

@NgModule({
    imports: [
		CommonModule,
        GetDocRouter,
        MaterialDesignModule,
        PdfViewerModule,
        DialogModule
	],
	declarations: [
		GetDocComponent,
	]
})
export class GetDocModule { }
