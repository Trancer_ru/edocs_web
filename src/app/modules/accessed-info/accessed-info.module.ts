import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { AccessedInfoComponent } from "./accessed-info.component";
import { MaterialDesignModule } from "@root/material.module";
import { MomentModule } from 'ngx-moment';

@NgModule({
	imports: [
		CommonModule,
        MaterialDesignModule,
        MomentModule
	],
	declarations: [
        AccessedInfoComponent,
	]
})
export class AccessedInfoModule { }
