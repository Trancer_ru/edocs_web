import { HttpClient } from "@angular/common/http";
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, Inject } from "@angular/core";
import {BehaviorSubject, Observable, Subject, throwError} from "rxjs";
import { DocumentAccess } from "@models/document-access";
import { MAT_DIALOG_DATA } from "@angular/material";
import {map, catchError} from "rxjs/internal/operators";

@Component({
	selector: "accessed-info",
	templateUrl: "accessed-info.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccessedInfoComponent implements OnDestroy, OnInit {
    private _destroyed$: Subject<boolean> = new Subject<boolean>();

    accessed$: Observable<DocumentAccess[]>;
    displayedColumns = [
        'date',
        'time',
        'ipAddress',
        'opened'
    ];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: {id: string},
        private _http: HttpClient
    ) {}

	ngOnDestroy() {
		this._destroyed$.next(true);
		this._destroyed$.complete();
	}
	ngOnInit() {
        this.getAccessList();
	}

    getAccessList(){
        this.accessed$ = this._http.get<DocumentAccess[]>(`{{api}}/access-list/${this.data.id}`)
            .pipe(
                map( data => data.map(item => new DocumentAccess(item))),
                catchError(err => {
                    return throwError(err);
                })
            )
    }
}
