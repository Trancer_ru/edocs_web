import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AddNewDocComponent } from "./add-new-doc.component";
import { MaterialDesignModule } from "@root/material.module";

@NgModule({
	imports: [
		CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialDesignModule
	],
	declarations: [
        AddNewDocComponent,
	]
})
export class AddNewDocModule { }
