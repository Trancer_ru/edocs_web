import { HttpClient } from "@angular/common/http";
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, Inject } from "@angular/core";
import {BehaviorSubject, Observable, Subject, throwError} from "rxjs";
import { Document } from "@models/document";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {addDays} from "@root/utils/date-utils";
import {takeUntil, catchError} from "rxjs/internal/operators";
import {MatDialogRef} from "@angular/material";
import {HttpHeaders} from "@angular/common/http";
import {FileInput} from "ngx-material-file-input";

import * as moment from "moment";

import {
    DateAdapter,
    NativeDateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE
} from "@angular/material/core";

const CUSTOM_DATE_FORMATS = {
    parse: {
        dateInput: { month: "short", year: "numeric", day: "numeric" }
    },
    display: {
        dateInput: "input",
        monthYearLabel: { year: "numeric", month: "short" },
        dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
        monthYearA11yLabel: { year: "numeric", month: "long" }
    }
};
const dateFormat = "DD MMMM, YYYY";
// date adapter formatting material2 datepickers label when a date is selected
export class CustomDateAdapter extends NativeDateAdapter {
    format(date: Date, displayFormat: Object): string {
        if (displayFormat === "input") {
            return moment(date).format(dateFormat);
        } else {
            return date.toDateString();
        }
    }
}

@Component({
	selector: "add-new-doc",
	templateUrl: "add-new-doc.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS },
        { provide: DateAdapter, useClass: CustomDateAdapter }
    ]
})
export class AddNewDocComponent implements OnDestroy, OnInit {

    currentDate: Date = addDays(new Date(), 14);
    minDate: Date = addDays(new Date(), 0);
    maxDate: Date = addDays(new Date(), 365);
    isLoading: boolean = false;
    errorText$ = new BehaviorSubject<string>(undefined);
    private _destroyed$: Subject<boolean> = new Subject<boolean>();
    form: FormGroup;

    constructor(
        private _fb: FormBuilder,
        private _http: HttpClient,
        private _dialogRef: MatDialogRef<AddNewDocComponent>
    ) {}

	ngOnDestroy() {
		this._destroyed$.next(true);
		this._destroyed$.complete();
	}
	ngOnInit() {
        this.form = this._fb.group({
            description: ["", [Validators.required, Validators.maxLength(100)]],
            addressee: ["", [Validators.required, Validators.maxLength(50)]],
            expirationDate: [{value: this.currentDate, disabled: true}, [Validators.required]],
            sameIp: [true, [Validators.required]],
            fileName: ["", [Validators.required]]
        });
	}

    add(){
        if(this.isLoading) return;
        this.errorText$.next(undefined);
        this.isLoading = true;

        let formData = new FormData();
        const form = this.form.getRawValue();
        Object.keys(form).forEach(key => {
            formData.append(key, key === 'expirationDate' ? this._changeDateFormat(form[key]) : form[key]);
        });

        if (form.fileName) {
            formData.append('file', form.fileName.files[0], form.fileName.files[0].name);
            formData.append('fileName', form.fileName.files[0].name);
        }

        const HttpUploadOptions = {
            headers: new HttpHeaders({ "Accept": "multipart/form-data" })
        };

        this._http.post<{filename: string}>('{{api}}/add-doc', formData, HttpUploadOptions)
            .pipe(
                takeUntil(this._destroyed$),
                catchError(err => {
                    this.isLoading = false;
                    this.errorText$.next("An error occurred while uploading file");
                    return throwError(err);
                })
            )
            .subscribe( data => {
                this.isLoading = false;
                this._dialogRef.close(data.filename);
            });
    }

    private _changeDateFormat(date: Date){
        return moment(date).format("YYYY-MM-DD");
    }
}
