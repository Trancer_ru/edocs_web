import { HttpClient } from "@angular/common/http";
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { BehaviorSubject, Subject, throwError} from "rxjs";
import { catchError, takeUntil } from "rxjs/operators";
import {AppConfig} from "@srv/config.service";
import {DialogService} from "@root/modules/dialog/dialog.service";
import {Login} from "@root/models/login";
import {StorageService} from "@srv/storage.service";

@Component({
	selector: "login",
	templateUrl: "login.component.html",
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnDestroy, OnInit {
    isLoading: boolean = false;
    private _destroyed$: Subject<boolean> = new Subject<boolean>();
	form: FormGroup;

	constructor(
		private _router: Router,
        private _config: AppConfig,
        private _fb: FormBuilder,
        private _dialogService: DialogService,
        private _http: HttpClient,
        private _ls: StorageService
	) {}

    ngOnInit() {
        this.form = this._fb.group({
            login: ["", [Validators.required]],
            password: ["", [Validators.required]],
        });
    }

	login(){
	    if(this.isLoading == true) return;
        this.isLoading = true;

        this._http.post<{accessToken:string}>('{{api}}/auth', new Login(this.form.getRawValue()))
            .pipe(
                takeUntil(this._destroyed$),
                catchError(err => {
                    this.isLoading = false;
                    this._dialogService.openAlert({
                        title: "Error",
                        text: "Incorrect login or password",
                        buttons: [
                            {
                                title: "Close"
                            }
                        ]
                    });
                    return throwError(err);
                })
            )
            .subscribe( loginData => {
                this._ls.setData("UserToken", loginData.accessToken);
                this._config.isAuth.next(true);
                this.isLoading = false;
                this._router.navigate(["./"]);
            });

    }

	ngOnDestroy() {
		this._destroyed$.next(true);
		this._destroyed$.complete();
	}
}
