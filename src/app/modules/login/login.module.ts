import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./login.component";
import { LoginRouter } from "./login.router";
import { MaterialDesignModule } from "@root/material.module";
import {DialogModule} from "@root/modules/dialog/dialog.module";

@NgModule({
    imports: [
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
        LoginRouter,
        MaterialDesignModule,
        DialogModule.forRoot()
	],
	declarations: [
		LoginComponent,
	]
})
export class LoginModule { }
