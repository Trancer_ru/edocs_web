export class Document {
    id: number = undefined;
	user: string = undefined;
	fileName: string = undefined;
    filePrefix: string = undefined;
	description: string = undefined;
    addressee: string = undefined;
    expirationDate: string = undefined;
    sameIp: boolean = undefined;
    opened: string = undefined;

    fileNameFull: string;

    constructor(data?: Partial<Document>) {
        if (!data) {
            return;
        }
        if ((data instanceof Document)) {
            Object.assign(this, data);
        } else {
            const newObject = new Document();
            Object.keys(newObject).forEach(key =>
                this[key] = (data[key] === undefined && !!newObject[key] === newObject[key]) ? newObject[key] : data[key]
            );
            this.fileNameFull = this.filePrefix+this.fileName;
		}
    }
    toJSON() {
        const data: any = {};
        Object.keys(new Document()).forEach(key =>
            data[key] = this[key]
        );
        return data;
    }
}
