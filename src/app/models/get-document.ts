export class GetDocument {
    file: string = undefined;
	error: 'nofile'|'expired' = undefined;

    constructor(data?: Partial<GetDocument>) {
        if (!data) {
            return;
        }
        if ((data instanceof GetDocument)) {
            Object.assign(this, data);
        } else {
            const newObject = new GetDocument();
            Object.keys(newObject).forEach(key =>
                this[key] = (data[key] === undefined && !!newObject[key] === newObject[key]) ? newObject[key] : data[key]
            );
		}
    }
}
