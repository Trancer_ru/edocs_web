export class Login {
    login: string = undefined;
	password: string = undefined;

    constructor(data?: Partial<Login>) {
        if (!data) {
            return;
        }
        if ((data instanceof Login)) {
            Object.assign(this, data);
        } else {
            const newObject = new Login();
            Object.keys(newObject).forEach(key =>
                this[key] = (data[key] === undefined && !!newObject[key] === newObject[key]) ? newObject[key] : data[key]
            );
		}
    }
    toJSON() {
        const data: any = {};
        Object.keys(new Login()).forEach(key =>
            data[key] = this[key]
        );
        return data;
    }
}
