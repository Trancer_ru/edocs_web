export class DocumentAccess {
    accessDate: string = undefined;
	ip: string = undefined;
    opened: boolean = undefined;

    constructor(data?: Partial<DocumentAccess>) {
        if (!data) {
            return;
        }
        if ((data instanceof DocumentAccess)) {
            Object.assign(this, data);
        } else {
            const newObject = new DocumentAccess();
            Object.keys(newObject).forEach(key =>
                this[key] = (data[key] === undefined && !!newObject[key] === newObject[key]) ? newObject[key] : data[key]
            );
		}
    }
    toJSON() {
        const data: any = {};
        Object.keys(new DocumentAccess()).forEach(key =>
            data[key] = this[key]
        );
        return data;
    }
}
