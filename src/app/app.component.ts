import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppConfig } from "@srv/config.service";
import {BehaviorSubject, Subject} from "rxjs";
import { AuthGuard } from "@srv/auth.service";
import { DialogService } from "@modules/dialog/dialog.service";
import {HelpersService} from "@srv/helpers.service";
import {takeUntil, filter, map} from "rxjs/internal/operators";

@Component({
  selector: 'app-root',
  template: `<mat-toolbar color="primary">
                <mat-toolbar-row>
                    <span>eDocs</span> 
                    <span class="fill-space"></span>
                    <ng-container *ngIf="isAuth | async">
                        <button mat-icon-button>
                            <mat-icon (click)="addNewDoc()" aria-hidden="false" aria-label="Add new doc">note_add</mat-icon>
                        </button>
                        <a mat-button  (click)="logout()">Exit</a>
                    </ng-container>
                </mat-toolbar-row>
            </mat-toolbar>
            <router-outlet></router-outlet>`
})
export class AppComponent implements OnDestroy {
    isAuth = new BehaviorSubject<boolean>(undefined);
    private _destroyed$: Subject<boolean> = new Subject<boolean>();
    constructor(
        private _config: AppConfig,
        private _helpers: HelpersService,
        private _authService: AuthGuard,
        private _dialogService: DialogService
    ){
        this.isAuth = this._config.isAuth;
    }

    logout(){
        this._authService.logout();
    }

    addNewDoc(){
        this._dialogService.openModule('add-new-doc')
            .pipe(
                filter(filename => Boolean(filename)),
                takeUntil(this._destroyed$)
            )
            .subscribe(filename => filename ? this._helpers.docAdded$.next(filename) : null);
    }

    ngOnDestroy() {
        this._destroyed$.next(true);
        this._destroyed$.complete();
    }
}
