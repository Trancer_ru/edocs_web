import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "@srv/auth.service";

const routes: Routes = [
	{ path: "", redirectTo: "list", pathMatch: "full" },

	{ path: "list", loadChildren: "./modules/docs-list/docs-list.module#DocsListModule", canLoad: [AuthGuard]},
    //{ path: "new-doc", loadChildren: "./modules/new-doc/new-doc.module#NewDocModule", canLoad: [AuthGuard]},
    { path: "login", loadChildren: "./modules/login/login.module#LoginModule"},
    { path: "get-document/:filename", loadChildren: "./modules/get-doc/get-doc.module#GetDocModule"}


	// todo: Отрубил, чтобы детектить ошибки урл переходов { path: "**", redirectTo: "login" }
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', preloadingStrategy: PreloadAllModules })],
	exports: [RouterModule],
	providers: []
})
export class AppRoutingModule { }



