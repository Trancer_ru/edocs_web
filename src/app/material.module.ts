import { NgModule } from '@angular/core';

import {
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule } from '@angular/material';
import { MaterialFileInputModule } from "ngx-material-file-input";

const modules: any[] = [
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MaterialFileInputModule
];

// Declare Module that imports/exports the @angular/material modules needed in the app
@NgModule({
    imports: [...modules],
    exports: [...modules]
})
export class MaterialDesignModule {}
