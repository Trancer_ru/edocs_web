import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./routing.module";
import { LocalStorageModule } from "angular-2-local-storage";
import { MaterialDesignModule } from "./material.module";
import { SERVICES } from "./services";
import { DialogModule } from "@modules/dialog/dialog.module";

import { environment } from "../environments/environment";
import { InitModule } from "@modules/starter-modules/initialize/init.module";
import {TOKEN_CONFIG_TOKEN} from "@root/interceptors/token/token.config";
import {TokenModule} from "@root/interceptors/token/token.module";
import { URI_RESOLVER_CONFIG_TOKEN } from "./interceptors/uri-resolver/uri-resolver.config";
import { UriResolverModule } from "./interceptors/uri-resolver/uri-resolver.module"
import {UnauthInterceptor} from "@root/services/unauth.interceptor";

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		AppRoutingModule,
        MaterialDesignModule,
        DialogModule.forRoot(),
        LocalStorageModule.forRoot({
            prefix: "edocs",
            storageType: "localStorage"
        }),
        TokenModule.forRoot(),
        UriResolverModule.forRoot(),
        InitModule
	],
	providers: [
        SERVICES,
        {
            provide: TOKEN_CONFIG_TOKEN,
            useValue: environment.tokenConfig
        },
        {
            provide: URI_RESOLVER_CONFIG_TOKEN,
            useValue: environment.uriResolverConfig
        },
        {
            provide: HTTP_INTERCEPTORS,
            useExisting: UnauthInterceptor,
            multi: true
        },
    ],
	bootstrap: [AppComponent]
})
export class AppModule {}
