import { Inject, Injectable } from '@angular/core';
import { StorageService } from '@srv/storage.service';
import { defaultTokenConfig, TOKEN_CONFIG_TOKEN } from './token.config';
import { ITokenConfig } from './token.interface';
import {HttpHeaders} from "@angular/common/http";
@Injectable()
export class TokenService {
  constructor(
    @Inject(TOKEN_CONFIG_TOKEN) private _jwtConfig: ITokenConfig,
    private _storageService: StorageService
  ) { }
  getHeaders(currentHeaders: HttpHeaders) {
    const headers = {};
    const headerName = (this._jwtConfig &&
      this._jwtConfig.headerName) || defaultTokenConfig.headerName;
    const headerPrefix = (this._jwtConfig &&
      this._jwtConfig.headerPrefix) || defaultTokenConfig.headerPrefix;
    const storageKeyName = (this._jwtConfig &&
      this._jwtConfig.storageKeyName) || defaultTokenConfig.storageKeyName;

    headers[headerName] = headerPrefix + ' ' + this._storageService.getData(storageKeyName);
    if(!currentHeaders.keys().length) headers['Content-Type'] = 'application/json';
    return headers;
  }
}
