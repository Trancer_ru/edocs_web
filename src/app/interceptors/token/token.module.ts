import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { TokenInterceptor } from './token.interceptor';
import { TokenService } from './token.service';


@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        TokenService,
        TokenInterceptor
    ]
})
export class TokenModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: TokenModule,
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useExisting: TokenInterceptor,
                    multi: true
                }
            ]
        }
    }
}
