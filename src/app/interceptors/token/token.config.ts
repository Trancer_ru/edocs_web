import { ITokenConfig } from "./token.interface";

export const defaultTokenConfig: ITokenConfig = {
  withTokenUriTemplates: [],
  headerName: 'Authorization',
  headerPrefix: 'JWT',
  storageKeyName: 'token'
};
export const TOKEN_CONFIG_TOKEN = 'TokenConfig';