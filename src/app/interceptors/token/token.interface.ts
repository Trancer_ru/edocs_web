export interface ITokenConfig {
    apiUri?: string;
    withTokenUriTemplates?: string[];
    headerName?: string;
    headerPrefix?: string;
    tokenName?: string;
    storageKeyName?: string;
  }