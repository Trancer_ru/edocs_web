import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { defaultTokenConfig, TOKEN_CONFIG_TOKEN } from './token.config';
import { ITokenConfig } from './token.interface';
import { TokenService } from './token.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        @Inject(TOKEN_CONFIG_TOKEN) private _jwtConfig: ITokenConfig,
        private _tokenService: TokenService
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const withTokenUriTemplates = (this._jwtConfig &&
            this._jwtConfig.withTokenUriTemplates &&
            this._jwtConfig.withTokenUriTemplates) || defaultTokenConfig.withTokenUriTemplates;
        if (
            withTokenUriTemplates.filter(rule => request.urlWithParams.indexOf(rule) !== -1).length > 0
        ) {
            request = request.clone({
                setHeaders: this._tokenService.getHeaders(request.headers)
            });
        }

        return next.handle(request);
    }
}
