import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UriResolverService } from './uri-resolver.service';

@Injectable()
export class UriResolverInterceptor implements HttpInterceptor {
    constructor(
        private _uriResolverService: UriResolverService
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const newUrl = this._uriResolverService.replaceUrl(request.url);
        if (
            request.url !== newUrl
        ) {
            const requestWithCustomUri = request.clone({ url: newUrl });
            return next.handle(requestWithCustomUri);
        }
        return next.handle(request);
    }
}
