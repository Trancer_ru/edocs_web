export interface IUriResolverConfigApiRule {
  uriTemplates?: string[];
  uri?: string;
}
export interface IUriResolverConfig {
  rules: IUriResolverConfigApiRule[];
}
