import { IUriResolverConfig } from "./uri-resolver.interface";

export const defaultUriResolverConfig: IUriResolverConfig = {
  rules: []
};
export const URI_RESOLVER_CONFIG_TOKEN = 'UriResolverConfig';