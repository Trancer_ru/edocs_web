import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { UriResolverInterceptor } from './uri-resolver.interceptor';
import { UriResolverService } from './uri-resolver.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    UriResolverInterceptor
  ]
})
export class UriResolverModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: UriResolverModule,
      providers: [
        UriResolverService,
        {
          provide: HTTP_INTERCEPTORS,
          useExisting: UriResolverInterceptor,
          multi: true
        }
      ]
    }
  }
}
