import { Inject, Injectable } from '@angular/core';
import { AppConfig } from '@srv/config.service';
import { defaultUriResolverConfig, URI_RESOLVER_CONFIG_TOKEN } from './uri-resolver.config';
import { IUriResolverConfig } from './uri-resolver.interface';

@Injectable()
export class UriResolverService {

    constructor(
        @Inject(URI_RESOLVER_CONFIG_TOKEN) private _uriResolverConfig: IUriResolverConfig,
        private _appConfig: AppConfig,
    ) {}
    replaceUrl(url: string) {
        const rules = (this._uriResolverConfig && this._uriResolverConfig.rules) || defaultUriResolverConfig.rules;
        let newUrl: string;
        rules.forEach(rule => {
            if (
                !newUrl &&
                rule.uriTemplates.length > 0 &&
                rule.uriTemplates.filter(template => url.indexOf(template) === 0).length > 0
            ) {
                newUrl = url.replace('{{api}}', this._appConfig.baseUrl);
            }
        });
        if (newUrl) {
            return newUrl;
        }
        return url;
    }
}
