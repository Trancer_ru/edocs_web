import { ITokenConfig } from "@root/interceptors/token/token.interface";
import { IUriResolverConfig } from "@root/interceptors/uri-resolver/uri-resolver.interface";

export interface IEnvironment {
    production: boolean;
    tokenConfig: ITokenConfig,
    uriResolverConfig: IUriResolverConfig
}
