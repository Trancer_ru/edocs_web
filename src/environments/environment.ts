import {IEnvironment} from "./environment.interface";

export const environment: IEnvironment = {
    production: false,
    tokenConfig: {
        headerName: 'authorization',
        headerPrefix: 'Bearer',
        storageKeyName: 'UserToken',
        withTokenUriTemplates: [
            '{{api}}/docs-list',
            '{{api}}/add-doc',
            '{{api}}/access-list'
        ]
    },
    uriResolverConfig: {
        rules: [
            {
                uriTemplates: [
                    '{{api}}/auth',
                    '{{api}}/docs-list',
                    '{{api}}/add-doc',
                    '{{api}}/get-doc',
                    '{{api}}/access-list'
                ]
            }
        ]
    }
}
